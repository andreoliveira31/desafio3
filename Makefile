all: km.o
	gcc -o km km.o -lm
	
km.o:
	gcc -c km.c
	
clean:
	rm km *.o
